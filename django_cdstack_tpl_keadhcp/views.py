import copy
import json

from django.template import engines

from django_cdstack_deploy.django_cdstack_deploy.func import (
    generate_config_static,
    zip_add_file,
)
from django_cdstack_tpl_deb_buster.django_cdstack_tpl_deb_buster.views import (
    handle as handle_deb_buster,
)
from django_cdstack_tpl_deb.django_cdstack_tpl_deb.views import handle as handle_deb
from django_cdstack_tpl_deb.django_cdstack_tpl_deb.views import get_os_release
from django_cdstack_tpl_deb_stretch.django_cdstack_tpl_deb_stretch.views import (
    get_apt_packages,
    get_apt_sources,
    get_apt_prefs,
)
from django_cdstack_tpl_crowdsec_main.django_cdstack_tpl_crowdsec_main.views import (
    handle as handle_crowdsec_main,
)
from django_cdstack_tpl_fail2ban.django_cdstack_tpl_fail2ban.views import (
    handle as handle_fail2ban,
)
from django_cdstack_tpl_ipcop.django_cdstack_tpl_ipcop.views import (
    handle as handle_ipcop,
)
from django_cdstack_tpl_wazuh_agent.django_cdstack_tpl_wazuh_agent.views import (
    handle as handle_wazuh_agent,
)
from django_cdstack_tpl_whclient.django_cdstack_tpl_whclient.views import (
    handle as handle_whclient,
)


def handle(zipfile_handler, template_opts, cmdb_host, skip_handle_os=False):
    django_engine = engines["django"]

    module_prefix = "django_cdstack_tpl_keadhcp/django_cdstack_tpl_keadhcp"

    if "apt_packages" not in template_opts:
        template_opts["apt_packages"] = get_apt_packages(
            module_prefix + "/templates/image/imageconf/vmtemplate.yml"
        )

    if "fail2ban_apps" not in template_opts:
        template_opts["fail2ban_apps"] = copy.deepcopy(template_opts["apt_packages"])

    if "apt_sources" not in template_opts:
        template_opts["apt_sources"] = get_apt_sources(
            module_prefix + "/templates/image/imageconf/vmtemplate.yml"
        )

    if "apt_preferences" not in template_opts:
        template_opts["apt_preferences"] = get_apt_prefs(
            module_prefix + "/templates/image/imageconf/vmtemplate.yml"
        )

    if "os_release" not in template_opts:
        template_opts["os_release"] = get_os_release(
            module_prefix + "/templates/image/imageconf/vmtemplate.yml"
        )

    if "dcmgrclient_sync_enable" not in template_opts:
        template_opts["dcmgrclient_sync_enable"] = "0"

    if template_opts["dcmgrclient_sync_enable"] != "0":
        dhcp4_json_file = open(
            module_prefix + "/templates/config-fs/dynamic/etc/kea/kea-dhcp4.conf.json"
        )
        dhcp4_data = json.load(dhcp4_json_file)

        dhcp4_data["Dhcp4"]["server-tag"] = template_opts["node_hostname_fqdn"]

        dhcp4_data["Dhcp4"]["config-control"]["config-databases"] = list()

        if "dbms_config_server" in template_opts:
            config_database = dict()
            config_database["type"] = "mysql"
            config_database["name"] = template_opts["dbms_config_db"]
            config_database["user"] = template_opts["dbms_config_username"]
            config_database["password"] = template_opts["dbms_config_password"]
            config_database["host"] = template_opts["dbms_config_server"]
            config_database["port"] = 3306

            dhcp4_data["Dhcp4"]["config-control"]["config-databases"].append(
                config_database
            )

            dhcp4_data["Dhcp4"]["lease-database"]["type"] = "mysql"
            dhcp4_data["Dhcp4"]["lease-database"]["name"] = template_opts[
                "dbms_config_db"
            ]
            dhcp4_data["Dhcp4"]["lease-database"]["user"] = template_opts[
                "dbms_config_username"
            ]
            dhcp4_data["Dhcp4"]["lease-database"]["password"] = template_opts[
                "dbms_config_password"
            ]
            dhcp4_data["Dhcp4"]["lease-database"]["host"] = template_opts[
                "dbms_config_server"
            ]
            dhcp4_data["Dhcp4"]["lease-database"]["port"] = 3306

            dhcp4_data["Dhcp4"]["hosts-database"]["type"] = "mysql"
            dhcp4_data["Dhcp4"]["hosts-database"]["name"] = template_opts[
                "dbms_config_db"
            ]
            dhcp4_data["Dhcp4"]["hosts-database"]["user"] = template_opts[
                "dbms_config_username"
            ]
            dhcp4_data["Dhcp4"]["hosts-database"]["password"] = template_opts[
                "dbms_config_password"
            ]
            dhcp4_data["Dhcp4"]["hosts-database"]["host"] = template_opts[
                "dbms_config_server"
            ]
            dhcp4_data["Dhcp4"]["hosts-database"]["port"] = 3306
        else:
            dhcp4_data["Dhcp4"]["lease-database"] = dict()
            dhcp4_data["Dhcp4"]["lease-database"]["type"] = "memfile"
            dhcp4_data["Dhcp4"]["lease-database"]["lfc-interval"] = 3600

        zip_add_file(
            zipfile_handler,
            "etc/kea/kea-dhcp4.conf",
            json.dumps(dhcp4_data, sort_keys=True, indent=4),
        )

    generate_config_static(zipfile_handler, template_opts, module_prefix)
    handle_whclient(zipfile_handler, template_opts, cmdb_host, skip_handle_os)
    handle_ipcop(zipfile_handler, template_opts, cmdb_host, skip_handle_os)
    handle_fail2ban(zipfile_handler, template_opts, cmdb_host, skip_handle_os)
    handle_crowdsec_main(zipfile_handler, template_opts, cmdb_host, skip_handle_os)
    handle_wazuh_agent(zipfile_handler, template_opts, cmdb_host, skip_handle_os)

    if not skip_handle_os:
        handle_deb(zipfile_handler, template_opts, cmdb_host, skip_handle_os)

    return True
